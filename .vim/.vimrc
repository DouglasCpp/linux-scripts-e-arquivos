execute pathogen#infect()

filetype plugin indent on
syntax on

"-------------CTRL+P"
set runtimepath^=~/.vim/bundle/ctrlp.vim

"------------Indentação"
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab
set smarttab
set smartindent
set autoindent
set number

"------------Fechar { sozinho, etc"
"inoremap { {<CR><BS>}<Esc>ko"
":inoremap ( ()<Esc>i"
"noremap        (  ()<Left>"
"inoremap <expr> )  strpart(getline('.'), col('.')-1, 1) == ")" ? "\<Right>" : ")"
"inoremap " ""<Esc>i"
"inoremap [ []<Esc>i"

"java autocomplete"
autocmd FileType java setlocal omnifunc=javacomplete#Complete
