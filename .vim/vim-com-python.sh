#https://vi.stackexchange.com/questions/11526/how-to-enable-python-feature-in-vim/17502
sudo apt install libncurses5-dev \
libgtk2.0-dev libatk1.0-dev \
libcairo2-dev python-dev \
python3-dev git checkinstall

sudo apt remove vim vim-runtime gvim

cd /usr && sudo git clone https://github.com/vim/vim.git && cd vim

sudo ./configure --with-features=huge \
--enable-multibyte \
--enable-pythoninterp=yes \
--with-python-config-dir=/usr/lib/python2.7/config-x86_64-linux-gnu/ \  # pay attention here check directory correct
--enable-python3interp=yes \
--with-python3-config-dir=/usr/lib/python3.5/config-3.5m-x86_64-linux-gnu/ \  # pay attention here check directory correct
--enable-gui=gtk2 \
--enable-cscope \
--prefix=/usr/local/

sudo make VIMRUNTIMEDIR=/usr/local/share/vim/vim81

cd /usr/vim && sudo checkinstall

sudo update-alternatives --install /usr/bin/editor editor /usr/local/bin/vim 1
sudo update-alternatives --set editor /usr/local/bin/vim
sudo update-alternatives --install /usr/bin/vi vi /usr/local/bin/vim 1
sudo update-alternatives --set vi /usr/local/bin/vim
