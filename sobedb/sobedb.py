#!/usr/bin/python3
import os, sys

path = os.path.dirname(__file__)

tipo = sys.argv[1]
if (tipo == "local") or (tipo == "l"):
	if(len(sys.argv) != 3):
		print("A função de subir bancos recebe apenas um argumento!")
		print("Execute 'sobedb l bancos' para listar os bancos disponíveis.")
		exit()
	path += "/sobedb_local " + sys.argv[2]
elif(tipo == "download") or (tipo == "d"):
	if(len(sys.argv) == 2):
		path += "/sobedb_download"
#	elif(len(sys.argv) == 3):
#		path += "/sobedb_download " + sys.argv[2]
	else:
		print("A função de extrair e mover bancos não recebe nenhum argumento!")
		exit()
else:
	print("Tipo inválido")
	exit()

os.system(path)
