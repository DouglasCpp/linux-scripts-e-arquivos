#!/bin/bash
if [[ $(whoami) != "root" ]] ; then
	echo "Você não é root!"
	exit
fi

# Remover link antigo
if [ -f "/usr/local/bin/sobedb" ]; then
	read -p "O arquivo /usr/local/bin/sobedb já existe. Deseja removê-lo? " REPLY
	if [[ $REPLY =~ ^[Ss] ]]
	then
		sudo rm /usr/local/bin/sobedb
		echo "Link removido."
	else
		# Não pode continuar se o arquivo existir e o usuário não quiser remover
		exit 0
	fi
fi

# Criar link para o script
echo "Criando link para o script em /usr/local/bin..."
sudo echo -e "python3 $(pwd)/sobedb.py \$1 \$2" > /usr/local/bin/sobedb
sudo chmod +x /usr/local/bin/sobedb
echo "Link criado."
echo "Instalação finalizada!"

