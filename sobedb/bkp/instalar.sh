#!/bin/bash
if [[ $(whoami) != "root" ]] ; then
	echo "Você não é root!"
	exit
fi

# Configuração inicial de pastas
if [ ! -f "pasta_bancos" ];
then
	read -p "Em qual pasta serão salvos os bancos(digite s para o padrão $HOME/DUMP)? "
	if [[ $REPLY =~ ^[Ss]$ ]]
	then
		echo "$HOME/DUMP" > "config/pasta_bancos"	
	else
		echo "$REPLY" > "config/pasta_bancos"
	fi
fi

if [ ! -f "pasta_downloads" ];
then
	read -p "Em qual pasta o seu navegador salva os bancos(digite s para o padrão $HOME/Downloads)? "
	if [[ $REPLY =~ ^[Ss]$ ]]
	then
		echo "$HOME/Downloads" > "config/pasta_downloads"	
	else
		echo "$REPLY" > "config/pasta_downloads"
	fi
fi

if [ ! -f "deletar_zip" ];
then
	read -p "Deseja que o script sempre delete os arquivos zip da pasta downloads(s/n)? "
	echo "$REPLY" > "config/deletar_zip"
fi

# Criar link para o script
echo "Criando link para o script em /usr/local/bin..."
sudo echo -e "python3 $(pwd)/sobedb.py \$1" > /usr/local/bin/sobedb
sudo chmod +x /usr/local/bin/sobedb
echo "Link criado."
echo "Instalação finalizada!"
