#!/usr/bin/python3
import pandas as pd
import sys

#argv1 = data e hora do inicio do ticket
#argv2 = data e hora do fim ticket
idx = pd.DatetimeIndex(["2016-01-01 " + sys.argv[1],
                        "2016-01-01 " + sys.argv[2]],
                       dtype='datetime64[ns]', name='date_time', freq=None)

print(idx.hour + (idx.minute / 60)) 
