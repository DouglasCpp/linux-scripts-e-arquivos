#!/usr/bin/python3
import sys

def decryptCaesar(message, mostCommonLetter):
    #Dictionary with the frequency of each character within the message
    freqDict = {}
    for c in message:
        if c != ' ':
            freqDict[c] = 1 if freqDict.get(c) == None else freqDict[c] + 1

    #print(freqDict)
    #Most frequent character
    mostCommonInDict = max(freqDict, key=freqDict.get)
    #print("mais frequente = <{}>".format(mostCommonInDict))
    
    diff = ord(mostCommonInDict) - ord(mostCommonLetter)
    
    #print("diff entre <{}> e <{}> = {}".format(mostCommonInDict, mostCommonLetter, diff))
    decryptedMsg = ''
    for c in message:
        if c != ' ':
            decryptedChr = chr((ord(c) - diff))
            if(ord(decryptedChr) < 97):
                decryptedMsg += chr((ord(decryptedChr) - 97) % 26 + 97)
            else:
                decryptedMsg += decryptedChr
            #print("{} = {}".format(c, ord(c) - diff))
        else:
            decryptedMsg += c
        
    return decryptedMsg
    

if __name__ == '__main__':
    print(decryptCaesar(sys.argv[1], sys.argv[2]))
    
    
