# linux-scripts-e-arquivos
Repositório com scripts, comandos e  anotações relacionadas principalmente a linux.
## Pasta sobedb
Script para automatizar a restauração de backups de bancos postgres
#### Instalar/desinstalar
Rodar o comando:
`./instalar.sh`

#### Movendo um banco baixado para o dump:
`sobedb d <numero ou nome do banco>`  
A opção de subir o banco será dada após extrair!

#### Subindo um banco do dump(já extraído)
`sobedb l <numero ou nome do banco>`

#### Subindo um banco dos arquivos Gerencial.sql e NFE.sql da pasta atual:
`sobedb l`

#### Lista de Bancos
A lista de bancos disponíveis é a seguinte:

```
1 - icode
2 - autocenter
3 - bijuchica
4 - infomania
5 - mocabiju
6 - projetos
```
### TODO
- Refatorar para remover códigos repetidos, modularizando as funcionalidades
- Verificação de erros após execução de cada comando, tirando a necessidade de confirmação do usuário após cada comando

## Pasta linux
Comandos úteis separados por função. A pasta sysadmin diz respeito a comandos relacionados com a administração do sistema, enquanto os outros arquivos são comandos mais genéricos.

## Pasta git
Comandos úteis para repositórios git
