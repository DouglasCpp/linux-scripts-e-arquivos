/*

Compilar:
$ javac Main.java

Executar:
$ java Main "Habilita a opção de numeração estatística"

Gerar lista de unicodes:
$ cat arquivo.properties | grep -o \\\\u00.. | sort -u > unicode.properties

*/
import java.util.*;  
import java.io.*;  
public class Main {

	public static void main(String[] args) throws Exception {
		FileReader reader = new FileReader("unicode.properties");
		Properties p = new Properties();
		p.load(reader);
		Set set = p.entrySet();
		Iterator itr=set.iterator();  
		String argumento = args[0];
		while(itr.hasNext()){  
			Map.Entry entry=(Map.Entry)itr.next();  
			argumento = argumento.replaceAll(entry.getValue().toString(), "\\"+entry.getKey().toString());
			//System.out.println(String.format("substituindo %s por %s",entry.getValue().toString() , entry.getKey().toString()));
		}
		System.out.println(argumento);
	}
}
